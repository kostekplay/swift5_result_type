////  ViewController.swift
//  ResultTypeInSwift5
//
//  Created on 06/06/2020.
//  Copyright © 2020 kostowski. All rights reserved.
//

import UIKit

struct Course: Decodable {
    let id: Int
    let name: String
    let imageUrl: String
    let number_of_lessons: Int
}

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchCourseJSON {(res) in
            switch res {
                case .success(let courses):
                    courses.forEach { (course) in
                        print(course.name)
                }
                case .failure(let err):
                    print("Failed to fetch courses. ", err)
            }
        }
    }
    
    func fetchCourseJSON(completion: @escaping (Result<[Course], Error>)->()){
        
        let urlString = "https://api.letsbuildthatapp.com/jsondecodable/courses"
        
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            
            if let err = err {
                completion(.failure(err))
                return
            }
            // successful
            do {
                let courses = try JSONDecoder().decode([Course].self, from: data!)
                completion(.success(courses))
            } catch let jsonError {
                completion(.failure(jsonError))
            }
        }.resume()
    }
}



